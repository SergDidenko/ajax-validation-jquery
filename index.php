<?php
if ( "POST" === $_SERVER['REQUEST_METHOD'] ) {
	if ( ! empty( $_POST ) ) {
		$name       = $_POST['name'];
		$email      = $_POST['mail'];
		$gender     = $_POST['gender'];
		$preference = 'male' == $gender ? $_POST['car'] : $_POST['flower'];
		// Validation of form data.
		$errors = [];
		if ( empty( $name ) || strlen( $name ) < 2 || ! preg_match( '/^[a-zA-Z]+$/', $name ) ) {
			$errors['#inputName'] = "The name is not valid.";
		}
		if ( empty( $email ) || ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			$errors['#inputEmail'] = "The email is not valid.";
		}
		if ( empty( $preference ) || strlen( $preference ) < 2 || ! preg_match( '/^[a-zA-Z ]+$/', $preference ) ) {
			if ( 'male' == $gender ) {
				$errors['#inputCar'] = "The value is not valid.";
			} else {
				$errors['#inputFlower'] = "The value is not valid.";
			}
		}
		if ( empty( $errors ) ) {
			// Send mail $to.
			$to      = 'serg@localhost';
			$subject = 'Contact information about user.';
			$message = 'Name: ' . $name . '<br>Gender: ' . $gender . '<br>Email: ' . $email . '<br>Preferences: ' . $preference;
			$headers = 'Content-type: text/html; charset="UTF-8" format=flowed';
			mail( $to, $subject, $message, $headers );
		} else {
			echo json_encode( $errors );
		}
		exit();
	}
}
require "templates/index.html";
