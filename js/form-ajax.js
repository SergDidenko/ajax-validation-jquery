$(document).ready(function () {
    $('input#inputName, input#inputEmail, input#inputCar, input#inputFlower').on('blur', function () {
        var id = $(this).attr('id');
        var val = $(this).val();
        switch (id) {
            case 'inputName':
                var reg = /^[a-zA-Z]+$/;
                if (val.length > 2 && val != '' && reg.test(val)) {
                    $(this).addClass('not_error');
                    $(this).next('.error-box').text('The name is valid.')
                        .css('color', '#00FF00')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').text('The name is not valid')
                        .css('color', '#FF0000')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
            case 'inputEmail':
                var reg = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
                if (val != '' && reg.test(val)) {
                    $(this).addClass('not_error');
                    $(this).next('.error-box').text('The email is valid.')
                        .css('color', '#00FF00')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').text('The email is not valid')
                        .css('color', '#FF0000')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
            case 'inputCar':
            case 'inputFlower':
                var reg = /^[a-zA-Z ]+$/;
                if (val != '' && val.length > 2 && reg.test(val)) {
                    $(this).addClass('not_error');
                    $(this).next('.error-box').text('The value is valid.')
                        .css('color', '#00FF00')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').text('The value is not valid')
                        .css('color', '#FF0000')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
        }
    });

    $('form#form-ajax').submit(function (e) {
        e.preventDefault();
        if ($('.not_error').length == 3) {
            $.ajax({
                url: 'index.php',
                type: 'post',
                data: $(this).serialize(),
                success: function (response) {
                    $('form#form-ajax :text').next('.error-box').text('');
                    if (response) {
                        response = JSON.parse(response);
                        for (var k in response) {
                            var el = 'input' + k;
                            $(el).removeClass('not_error').addClass('error');
                            $(el).next('.error-box').text(response[k])
                                .css('color', 'red')
                                .animate({'paddingLeft': '10px'}, 400)
                                .animate({'paddingLeft': '5px'}, 400);
                        }
                    }
                    else {
                        $("#dialog-message").dialog({
                            modal: true,
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                },
            });
        }
        else {
            return false;
        }
    });
    $('button#reset-button').click(function () {
        $('form#form-ajax :text').val('').next('.error-box').text('');
    });

});